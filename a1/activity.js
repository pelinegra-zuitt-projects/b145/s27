//Use the count operator to count the total number of fruits on sale

db.fruits.aggregate(
    [ { $match : { "onSale" : true } },
      {$count: "totalFruits"}
     ]);



//use the count operator to count the total number of fruits with stock more than 20


db.fruits.aggregate([ 
  {$match : { "stock" : {$gt:20}}}, 
  {$count: "fruitsmorethantwenty"}
]);

//use the average operator to get the average price of fruits onSale per supplier

db.fruits.aggregate([ 
  {$match : { "onSale" : true }}, 
  {
  	$group: {
  	_id: "$supplierID",
  	avg_price: { $avg: "$price"}
  }
 }
]); 

//use the max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([ 
  {$match : { "onSale" : true }}, 
  {
  	$group: {
  	_id: "$supplierID",
  	max_price: { $max: "$price"}
  }
 }
]); 

//use the min operator to get the lowest price of a fruit per supplier

db.fruits.aggregate([ 
  {$match : { "onSale" : true }}, 
  {
  	$group: {
  	_id: "$supplierID",
  	min_price: { $min: "$price"}
  }
 }
]); 