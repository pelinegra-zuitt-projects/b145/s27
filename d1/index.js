db.fruits.insertMany(
[
  {
      "name" : "Apple",
      "color" : "Red",
      "stock" : 20.0,
      "price" : 40.0,
      "onSale" : true,
      "origin" : [ 
          "Philippines", 
          "US"
      ]
  },
    {   
      "name" : "Banana",
      "color" : "Yellow",
      "stock" : 15.0,
      "price" : 20.0,
      "onSale" : true,
      "origin" : [ 
          "Philippines", 
          "Ecuador"
      ]
  },
    {
     
      "name" : "Kiwi",
      "color" : "Green",
      "stock" : 25.0,
      "price" : 50.0,
      "onSale" : true,
      "origin" : [ 
          "US", 
          "China"
      ]
  },
    {
     
      "name" : "Mango",
      "color" : "Yellow",
      "stock" : 10.0,
      "price" : 120.0,
      "onSale" : false,
      "origin" : [ 
          "Philippines", 
          "India"
      ]
    }
  ]
);


//Aggregation in MongoDB
//needed when your application needs a form of info that is not visibly available from your docs
//Aggregation pipeline ---> framework for data aggregation

//Basic syntax

db.collection.aggregate([
  {stageA},
  {stageB},
  {stageC}
])

//$match
// -- filters docs to pass only the docs matching the spec condition to the next pipeline stage
{$match: {query document}}

// mini activity
group together fruits that are currently on sale
db.fruits.aggregate(
    [ { $match : { "onSale" : true } } ]
);

//count
//{$count: <string>} ---string must be non-empty and does not have $

//miniactivity
//after grouping documents according to fruits currently on sale, count how many docs were returned by assigning it to fruitCount name

db.fruits.aggregate(
    [ { $match : { "onSale" : true } },
      {$count: "fruitCount"}
     ]);


// //GROUP --groups input docs by a specified identifier expression --applies accumulator expression
{
  $group:
    {
      _id: <expression>, // Group By Expression
      <field1>: { <accumulator1> : <expression1> },
      ...
    }
 }

//Miniactivity
// after grouping docs according to fruits currently on sale, 
// group docs according to null and get the total sum of the stock field then assign it to "total" field 
//{use $sum accumulator operator}

db.fruits.aggregate([ 
  {$match : { "onSale" : true }}, 
  {
    $group: {
      _id: null, 
      total: { $sum: "$stock"}
    }
  }
]); 
//mini act
//group together fruits that has more than 10 stocks and 
// then exclude  id in the returning doc

db.fruits.aggregate([ 
  {$match : { "stock" : {$gt:10}}}, 
  {$project: {_id: 0}}
]);

//SORT
//sort returned docs in ascending order according to price

//ascending
db.fruits.aggregate([ 
  {$match : { "stock" : {$gt:10}}}, 
  {$project: {_id: 0}},
  {$sort: {price: 1}}
]);

//descending
db.fruits.aggregate([ 
  {$match : { "stock" : {$gt:10}}}, 
  {$project: {_id: 0}},
  {$sort: {price: -1}}
]);